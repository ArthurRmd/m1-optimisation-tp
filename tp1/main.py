import math
import timeit

import numpy as np


def calculFn(x1, x2):
    return (x1 - 3) ** 2 + (x2 + 1) ** 2


def calculFn2(x1, x2):
    return (x1 + (-2 * x2) + 3) ** 2 + (4 * x1 + x2 - 1) ** 2


def exo1():
    minimum = math.inf

    minNumber = -2000
    maxNumber = 2000

    for i in range(minNumber, maxNumber):

        i = i * 0.01
        first = (i - np.pi) ** 2

        for j in range(minNumber, maxNumber):
            j = j * 0.01
            second = (j + math.sqrt(2)) ** 2

            total = first + second

            if total < minimum:
                minimum = total
                minX1 = i
                minX2 = j

    print(minimum)
    print('min i  : ', minX1)
    print('min j  : ', minX2)


def exo1V2():
    minimumFirst = math.inf
    minimumSecond = math.inf
    minNumber = -2000000
    maxNumber = 2000000

    for i in range(minNumber, maxNumber):
        i = i * 0.001
        first = (i - np.pi) ** 2
        if first < minimumFirst:
            minimumFirst = first
            minX1 = i

    for i in range(minNumber, maxNumber):
        i = i * 0.001
        second = (i + math.sqrt(2)) ** 2
        if second < minimumSecond:
            minimumSecond = second
            minX2 = i


    print(minimumFirst + minimumSecond)
    print('min i  : ', minX1)
    print('min j  : ', minX2)




def exo2():
    start = 0
    step = 0.01

    for i in range(1,1000000):
        result = getX1(start)
        if result < 0:
            start += step
        else:
            start -= step

    print(start)


def getX1(x1):
    return 2*x1 - 10 * ( 2 * math.pi - math.sin(2 * math.pi * x1))

def getX2(x2):
    return 2*x2 - 10 * ( 2 * math.pi - math.sin(2 * math.pi * x2))


def main():
    minimum = math.inf

    minNumber = -10000
    maxNumber = 0

    for i in range(minNumber, maxNumber):

        for j in range(minNumber, maxNumber):

            res = calculFn2(i, j)

            if res < minimum:
                minimum = res
                minX1 = i
                minX2 = j

    print(minimum)
    print('min i  : ', minX1)
    print('min j  : ', minX2)



if __name__ == '__main__':
    start = timeit.default_timer()
    exo2()
    stop = timeit.default_timer()
    print('Time: ', stop - start)
