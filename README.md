m1-optimisation-tp

Arthur REMOND M1 I2L
##TP : A rendre pour le 25/02/2021

Enoncé : 
1) implementez un (1+1)-ES avec adaptation du pas, et évaluez le sur la fonction f1 avec un plt.semilogy en tracant 
    l'évolution de f(x_t) par rapport à t.

2) implementez un (mu,lambda)-ES avec adaptation du pas, et évaluez le sur f5 avec un tracé comme avant par rapport
    au (1+1). Que se passe-t-il à votre avis ?

3) considérez la fonction f_1 sous avec la contrainte que x_1 >= 0 et x_2 >= 0. Optimisez ce problème avec la 
    méthode de votre choix. Pour gérer la contrainte, vous pouvez : utiliser une méthode pénalisant tout x ne
    respectant pas la contrainte / "réparer" le point ne respectant pas la contrainte en le modifiant afin qu'il
    la respecte. Tracez l'évolution de f_1 comme avant.



#### Exercice 1 : 

Voici les resultats pour 100 et 1000 itérations (avec semilogy) avec sigma égale à 6 et gamma égale à 1.5.
On remarque que le résultat de f1(x) tends vers 10^⁻29 (pour 1000 itérations).
L'optimum trouvé est x1= 3 et x2=-1.

<p align="center">
    <img src="tp_es/results_img/es1plus1_100.png"  alt="m100-time-fitness-ils-ils_ie">
</p>
<p align="center">
    <img src="tp_es/results_img/es1plus1_1000.png"  alt="m100-time-fitness-ils-ils_ie">
</p>

Voici le resultat pour f5:
<p align="center">
    <img src="tp_es/results_img/es1plus1_f5_100.png"  alt="m100-time-fitness-ils-ils_ie">
</p>

#### Exercice 2 : 

Voici les resultats pour la stratégie d'évolution (mu,lambda) ES.
La valeur de mon sigma est égale à 6 et mon gamma est égale à 1.5.

<p align="center">
    <img src="tp_es/results_img/es_mu_lambda_f1_100.png"  alt="m100-time-fitness-ils-ils_ie">
</p>

<p align="center">
    <img src="tp_es/results_img/es_mu_lambda_f1_1000.png"  alt="m100-time-fitness-ils-ils_ie">
</p>

On remarque que pour f1 (mu,lambda) ES converge beaucoup plus rapidement que (1+1) ES.
Pour 100 itérations nous obtenons un résultat proche de 10^-1 pour (1+1) ES contre 10^-18 pour (mu,lambda) ES.
De même on remarque également que pour f1 le minimum global est trouvé après 200 itérations contre 
environ 650 pour (1+1) ES

<p align="center">
    <img src="tp_es/results_img/es_mu_lambda_f5_100.png"  alt="m100-time-fitness-ils-ils_ie">
</p>

Pour f5 avec (mu,lambda) ES, le resultat converge après 15 itérations.


#### Exercice 3 : 

Pour l'exercice 3 j'ai utilisé l'algorithme (1+1) ES (le même que celui de l'exercice n°1).
J'ai mis en place une contraint ou :
- si mon point x1 est inférieur à 0 alors je remplace sa valeur par sa valeur absolue
- si mon point x2 est inférieur à 0 alors je remplace sa valeur par sa valeur absolue

Avec cette contrainte je ne pourrais jamais avoir de valeur pour x1 et x2 qui sont inférieur à 0.

Après application de la contrainte, on remarque que l'algorithme (1+1) ES tends vers la puissance 10⁰ (même après 10 000 itérations)alors que celui 
sans contrainte tends vers 10^⁻29 (pour 1000 ittérations avec sigma égale à 6 et gamma égale à 1.5, pour la fonction f1(x)).
C'est résultat semble cohérents car les optimum trouvé par (1+1) ES sans contrainte sont x1=3 et x2=-1.
( ce de (1+1) ES avec contrainte sont x1=3 et x2=0).
Comme -1 ne se situe pas dans un espace de recherche valide (ne respecte pas la contraintes), (1+1) ES avec contrainte 
ne pourra jamais tomber dedans

<p align="center">
    <img src="tp_es/results_img/es1plus1_constraint_100.png"  alt="m100-time-fitness-ils-ils_ie">
</p>

<p align="center">
    <img src="tp_es/results_img/es1plus1_constraint_1000.png"  alt="m100-time-fitness-ils-ils_ie">
</p>

<p align="center">
    <img src="tp_es/results_img/es1plus1_constraint_10000.png"  alt="m100-time-fitness-ils-ils_ie">
</p>