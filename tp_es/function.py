import math
import random

import numpy as np
import matplotlib.pyplot as plt


def show_graph(x, y, title = ''):
    plt.figure()
    plt.title(title)
    plt.plot(x, y)
    plt.xlabel('nb iterations')
    plt.ylabel('y')
    plt.show()

def show_graph_semilogy(x, y, title = ''):
    plt.figure()
    plt.title(title)
    plt.semilogy(x, y)
    plt.xlabel('nb iterations')
    plt.ylabel('y')
    plt.show()


def f1(x):
    x1 = x[0]
    x2 = x[1]
    return (x1 - 3) ** 2 + (x2 + 1) ** 2

def f5(x):
    x1 = x[0]
    x2 = x[1]
    return x1**2 + x2**2 - 10 * (math.cos(2 * math.pi * x1)) + math.cos(2*math.pi*x2)

def getY(x, sigma):
    value = x + sigma * np.random.randn(2)
    return value


def getSeveralY(x, sigma, muValue, number_points, function = 'f1'):
    points = []

    for i in range(number_points):

        y = getY(x, sigma)
        if function == 'f1':
            y = np.append(y, f1(y))
        else:
            y = np.append(y, f5(y))

        points.append(y)

    points = sorted(points, key=lambda a_entry: a_entry[2])

    points = points[:muValue]
    return points


def generatePoints(numbers_points):
    x = []
    for j in range(numbers_points):
        x.append((100, 100))
    return x