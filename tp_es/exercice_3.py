from graph import Graph
from tp_es.function import getY, f1, show_graph

graph = Graph()


def esOnePlusOneWithConstraint(nb_iters):
    x = (100, 100)
    gamma = 1.2
    sigma = 2

    for i in range(nb_iters):
        y = getY(x, sigma)

        if (y[0] < 0):
            y[0] = abs(y[0])

        if (y[1] < 0):
            y[1] = abs(y[1])


        if f1(y) < f1(x):
            sigma = sigma * gamma
            x = y
        else:
            sigma = sigma * (gamma ** (-1 / 4))

        graph.append(i,f1(x))


    print(x)
    print(f1(x))
    graph.show_semilogy('(1+1) with constraint f1')