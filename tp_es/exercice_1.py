from graph import Graph
from tp_es.function import getY, f1, show_graph

graph = Graph()

def esOnePlusOne(nb_iters):
    x = (100, 100)
    gamma = 1.5
    sigma = 6

    for i in range(nb_iters):
        y = getY(x, sigma)

        if f1(y) < f1(x):
            sigma = sigma * gamma
            x = y
        else:
            sigma = sigma * (gamma ** (-1 / 4))

        graph.append(i,f1(x))



    print(x)
    print(f1(x))
    graph.show_semilogy('(1+1) without constraint f1')