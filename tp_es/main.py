from tp_es.exercice_1 import esOnePlusOne
from tp_es.exercice_2 import esMuLambda
from tp_es.exercice_3 import esOnePlusOneWithConstraint

if __name__ == '__main__':

    # esOnePlusOne(1000)
    esMuLambda(5, 100,'f1')
    # esMuLambda(5, 100,'f5')
    # esOnePlusOneWithConstraint(100)
