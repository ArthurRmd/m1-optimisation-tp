import math

import numpy as np

from graph import Graph
from tp_es.function import getSeveralY, f1, show_graph, generatePoints, f5

graph = Graph()


def esMuLambda(muValue, nb_iteration, function = 'f1'):

    n=2
    lambdaValue = (int)(4 + 3 * math.log(2))
    x = np.array([100,100])
    sigma = 30

    # initialise les poids
    w = np.array([math.log(muValue + 0.5) - math.log(i) for i in range(1, muValue + 1)])
    w = w / np.sum(w)

    sum_w_determinant = 0
    sum_w_denominateur = 0

    for i in range(muValue):
        sum_w_determinant += w[i]

    for i in range(muValue):
        sum_w_denominateur += (w[i] * w[i])

    uw = (sum_w_determinant**2) / sum_w_denominateur

    c = (2 + uw) / (n+5+uw)
    p = np.array([0,0])


    for i in range(nb_iteration):

        y = getSeveralY(x, sigma, muValue, lambdaValue, function)

        a = 0
        b = 0

        for j in range(muValue):
            a += w[j] * y[j][0]
            b += w[j] * y[j][1]

        # print(x)
        new_x = np.array([a,b])

        z = math.sqrt(uw) * ((new_x - x) / sigma)

        x = new_x
        p = ((1 - c) * p) + (math.sqrt(c * (2 - c)) * z)

        sigma = sigma * math.exp(c * (  ((p[0]**2) + (p[1]**2)) / n -1 )  )



        if function == 'f1':
            graph.append(i,f1(x))
        else:
            graph.append(i,f5(x))

    if function == 'f1':
        graph.show_semilogy('(mu,lambda) ES without constraint f1')
    else:
        graph.show('(mu,lambda) ES without constraint f5')