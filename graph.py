from tp_es.function import show_graph, show_graph_semilogy


class Graph:

    graphX = []
    graphY = []

    def append(self,x,y):
        self.graphX.append(x)
        self.graphY.append(y)

    def show(self, title):
        show_graph(self.graphX, self.graphY, title)

    def show_semilogy(self, title):
        show_graph_semilogy(self.graphX, self.graphY, title)
